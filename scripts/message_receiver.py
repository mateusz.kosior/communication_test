#!/usr/bin/env python

# Silesian University of Technology, 2021
# Author: Mateusz Kosior (mateusz.kosior@polsl.pl)

import rospy
import time
import ntplib
import time
from tabulate import tabulate
from std_msgs.msg import Header
from communication_test.msg import TimestampedBool, TimestampedString, \
    TimestampedInt8, TimestampedInt16, TimestampedInt32, TimestampedInt64, \
    TimestampedUInt8, TimestampedUInt16, TimestampedUInt32, TimestampedUInt64, \
    TimestampedFloat32, TimestampedFloat64

class ReceiverNode(object):

    def callback(self, msg):
        latency_ms = ((time.time() + self.time_correction) - msg.header.stamp.to_sec()) * 1000
        if not msg.header.frame_id in self.message_stats:
            self.message_stats[msg.header.frame_id] = {
                'Frame number' : list(),
                'Latency [ms]' : list(),
                'Frame data'   : list(),
            }
        frame = self.message_stats[msg.header.frame_id]
        frame['Frame number'].append(msg.header.seq)
        frame['Latency [ms]'].append(latency_ms)
        frame['Frame data'].append(msg.data)

    def __init__(self, frequency_Hz=1, seed=None, ntp_server='europe.pool.ntp.org'):
        
        rospy.init_node('Receiver', anonymous=True, log_level=rospy.INFO)
        
        # Configure the NTP client for time synch
        client = ntplib.NTPClient()
        ntp_time = client.request(ntp_server).tx_time
        local_time = time.time()
        self.time_correction = ntp_time - local_time
        rospy.loginfo('Synched with %s' % ntp_server)

        self.message_stats = dict()

        self.sub_bool = rospy.Subscriber('/sender/TimestampedBool',
            TimestampedBool, self.callback)
        self.sub_string = rospy.Subscriber('/sender/TimestampedString',
            TimestampedString, self.callback)
        self.sub_int8 = rospy.Subscriber('/sender/TimestampedInt8',
            TimestampedInt8, self.callback)
        self.sub_int16 = rospy.Subscriber('/sender/TimestampedInt16',
            TimestampedInt16, self.callback)
        self.sub_int32 = rospy.Subscriber('/sender/TimestampedInt32',
            TimestampedInt32, self.callback)
        self.sub_int64 = rospy.Subscriber('/sender/TimestampedInt64',
            TimestampedInt64, self.callback)
        self.sub_uint8 = rospy.Subscriber('/sender/TimestampedUInt8',
            TimestampedUInt8, self.callback)
        self.sub_uint16 = rospy.Subscriber('/sender/TimestampedUInt16',
            TimestampedUInt16, self.callback)
        self.sub_uint32 = rospy.Subscriber('/sender/TimestampedUInt32',
            TimestampedUInt32, self.callback)
        self.sub_uint64 = rospy.Subscriber('/sender/TimestampedUInt64',
            TimestampedUInt64, self.callback)
        self.sub_float32 = rospy.Subscriber('/sender/TimestampedFloat32',
            TimestampedFloat32, self.callback)
        self.sub_float64 = rospy.Subscriber('/sender/TimestampedFloat64',
            TimestampedFloat64, self.callback)

        rospy.loginfo('Receiver node initiated')

    def summarize(self):
        if len(self.message_stats) <= 0:
            return

        summary = [['', '', 'Latency [ms]', '', '', 'Frames', ''],
                  ['Type', 'Avg', 'Min', 'Max', 'Sent', 'Received', 'Lost']]

        avgs = list()
        mins = list()
        maxes = list()
        total_sent = 0
        total_received = 0

        for messageType, records in self.message_stats.items(): 
            # Try to compute the total number of sent frames using stored frame numbers
            sent = 0
            previous_frame_number = None
            for frame_number in records['Frame number']:
                if previous_frame_number and (frame_number > previous_frame_number):
                    sent += frame_number - previous_frame_number
                else:
                    sent += 1
                previous_frame_number = frame_number
            received = len(records['Frame number'])

            # Compute statistical metrics
            avg = sum(records['Latency [ms]']) / len(records['Latency [ms]'])
            minimum = min(records['Latency [ms]'])
            maximum = max(records['Latency [ms]'])
            summary.append([messageType, '%.3f' % avg, '%.3f' % minimum, '%.3f' % maximum,
                '%u' % sent, '%u' % received, '%.2f%%' % ((1 - (received / sent)) * 100)])

            # Add to the summarized result
            avgs.append(avg)
            mins.append(minimum)
            maxes.append(maximum)
            total_sent += sent
            total_received += received
        
        # Compute grand total
        summary.append(['TOTAL', '%.3f' % (sum(avgs) / len(avgs)), '%.3f' % min(mins),
            '%.3f' % max(maxes), '%u' % total_sent, '%u' % total_received,
            '%.2f%%' % ((1 - (total_received / total_sent)) * 100)])

        # Print the results
        print(tabulate(summary, tablefmt='plain'))
            

if __name__ == '__main__':

    ntp_server = 'europe.pool.ntp.org'
    receiver = ReceiverNode(ntp_server)
    
    rospy.spin()

    receiver.summarize()

    rospy.loginfo('Receiver terminated')
