#!/usr/bin/env python

# Silesian University of Technology, 2021
# Author: Mateusz Kosior (mateusz.kosior@polsl.pl)

import rospy
import time
import random
import ntplib
import time
from std_msgs.msg import Header, Time
from communication_test.msg import TimestampedBool, TimestampedString, \
    TimestampedInt8, TimestampedInt16, TimestampedInt32, TimestampedInt64, \
    TimestampedUInt8, TimestampedUInt16, TimestampedUInt32, TimestampedUInt64, \
    TimestampedFloat32, TimestampedFloat64

class SenderNode(object):

    def __init__(self, frequency_Hz=1, seed=None, ntp_server='europe.pool.ntp.org'):
        random.seed(seed)

        self.pub_bool = rospy.Publisher('/sender/TimestampedBool', TimestampedBool,
            queue_size = 1)
        self.pub_string = rospy.Publisher('/sender/TimestampedString', TimestampedString,
            queue_size = 1)
        self.pub_int8 = rospy.Publisher('/sender/TimestampedInt8', TimestampedInt8,
            queue_size = 1)
        self.pub_int16 = rospy.Publisher('/sender/TimestampedInt16', TimestampedInt16,
            queue_size = 1)
        self.pub_int32 = rospy.Publisher('/sender/TimestampedInt32', TimestampedInt32,
            queue_size = 1)
        self.pub_int64 = rospy.Publisher('/sender/TimestampedInt64', TimestampedInt64,
            queue_size = 1)
        self.pub_uint8 = rospy.Publisher('/sender/TimestampedUInt8', TimestampedUInt8,
            queue_size = 1)
        self.pub_uint16 = rospy.Publisher('/sender/TimestampedUInt16', TimestampedUInt16,
            queue_size = 1)
        self.pub_uint32 = rospy.Publisher('/sender/TimestampedUInt32', TimestampedUInt32,
            queue_size = 1)
        self.pub_uint64 = rospy.Publisher('/sender/TimestampedUInt64', TimestampedUInt64,
            queue_size = 1)
        self.pub_float32 = rospy.Publisher('/sender/TimestampedFloat32', TimestampedFloat32,
            queue_size = 1)
        self.pub_float64 = rospy.Publisher('/sender/TimestampedFloat64', TimestampedFloat64,
            queue_size = 1)

        rospy.init_node('Sender', anonymous=True, log_level=rospy.INFO)
        
        if seed:
            rospy.loginfo('seed = %i' % seed)
        else:
            rospy.loginfo('seed = generated')

        self.rate = rospy.Rate(frequency_Hz)
        rospy.loginfo('frequency = %.3f Hz' % frequency_Hz)

        self.numeric_limits = { 'int8'  : (-128, 127),
                                'int16' : (-32768, 32767),
                                'int32' : (-2147483648, 2147483647),
                                'int64' : (-9223372036854775808, 9223372036854775807),
                               'uint8'  : (0, 255),
                               'uint16' : (0, 65535),
                               'uint32' : (0, 4294967295),
                               'uint64' : (0, 18446744073709551615),
                               'float32': (1.175494351e-38, 3.402823466e+38),
                               'float64': (2.2250738585072014e-308, 1.7976931348623157e+308)}
        msg = 'Numeric limits for messages set as follows:'
        for key, value in self.numeric_limits.items():
            msg += '\n\t+ ' + key + ': ' + str(value[0]) + ' to ' + str(value[1])

        rospy.loginfo(msg)

        # Configure the NTP client for time synch
        client = ntplib.NTPClient()
        ntp_time = client.request(ntp_server).tx_time
        local_time = time.time()
        self.time_correction = ntp_time - local_time
        rospy.loginfo('Synched with %s' % ntp_server)

        rospy.loginfo('Sender node initiated')

    def run(self):
        while not rospy.is_shutdown():
            msg_time = rospy.Time.from_seconds(time.time() + self.time_correction)
            self.pub_bool.publish(TimestampedBool(
                header=Header(stamp=msg_time, frame_id='Bool'),
                data=random.choice((True, False))))
            msg_str = ''
            for i in range(0, random.randint(1, 50)):
                msg_str += random.choice(('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ\
                    01234567890`~!@#$%^&*()-_=+\\|\t\n[]{};:\'\",./<>?'))
            self.pub_string.publish(TimestampedString(header=Header(stamp=msg_time,
                frame_id='String'), data=msg_str))
            self.pub_int8.publish(TimestampedInt8(header=Header(stamp=msg_time,
                frame_id='Int8'), data=random.randint(*self.numeric_limits['int8'])))
            self.pub_int16.publish(TimestampedInt16(header=Header(stamp=msg_time,
                frame_id='Int16'), data=random.randint(*self.numeric_limits['int16'])))
            self.pub_int32.publish(TimestampedInt32(header=Header(stamp=msg_time,
                frame_id='Int32'), data=random.randint(*self.numeric_limits['int32'])))
            self.pub_int64.publish(TimestampedInt64(header=Header(stamp=msg_time,
                frame_id='Int64'), data=random.randint(*self.numeric_limits['int64'])))
            self.pub_uint8.publish(TimestampedUInt8(header=Header(stamp=msg_time,
                frame_id='UInt8'), data=random.randint(*self.numeric_limits['uint8'])))
            self.pub_uint16.publish(TimestampedUInt16(header=Header(stamp=msg_time,
                frame_id='UInt16'), data=random.randint(*self.numeric_limits['uint16'])))
            self.pub_uint32.publish(TimestampedUInt32(header=Header(stamp=msg_time,
                frame_id='UInt32'), data=random.randint(*self.numeric_limits['uint32'])))
            self.pub_uint64.publish(TimestampedUInt64(header=Header(stamp=msg_time,
                frame_id='UInt64'), data=random.randint(*self.numeric_limits['uint64'])))
            self.pub_float32.publish(TimestampedFloat32(header=Header(stamp=msg_time,
                frame_id='Float32'), data=random.uniform(*self.numeric_limits['float32'])))
            self.pub_float64.publish(TimestampedFloat64(header=Header(stamp=msg_time,
                frame_id='Float64'), data=random.uniform(*self.numeric_limits['float64'])))
            
            self.rate.sleep()


if __name__ == '__main__':

    frequency = 1 # in Hz
    seed = None # auto-generated if None
    ntp_server = 'europe.pool.ntp.org'
    sender = SenderNode(frequency, seed, ntp_server)
    
    try:
        sender.run()
    except rospy.ROSInterruptException:
        pass

    rospy.loginfo('Sender terminated')
